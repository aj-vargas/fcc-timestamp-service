Receives either a UNIX timestamp (in milliseconds) or a natural language date and returns the value in both representations.

Natural language dates received are parsed using the [Date constructor](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date) and if no timezone is specified, server's local time (probably UTC) is inferred.
