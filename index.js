'use strict';

const express = require('express');

const server = express();

server.get('/', (request, response) => {
  response.sendFile('timestamp.html', {root: 'public'});
});

server.get('/api/timestamp/:input', (request, response) => {
  let output = {
    unix: null,
    natural: null,
    input: request.params.input
  };
  let date;
  if (!output.input) {
    date = new Date();
  } else if (output.input.match(/\D/)) {
    date = new Date(output.input);
  } else {
    date = new Date(+output.input);
  }
  if (Number.isNaN(date.valueOf())) {
    output = {error: 'Invalid Date'};
  } else {
    output.unix = date.valueOf();
    output.natural = date.toUTCString();
  }
  response.json(output);
});

server.listen(8080);
